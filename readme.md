# Currency convertor
You must create a website and a webservice that allows the user to convert between two currencies.\
Below I describe how to use this set up in the local environment.\
For the prod/dev deployment, We need to setup the correct environment variables usage and create an automatic pipeline that will build and deploy images to the remote environment.\
Currently a lot of values are hardcoded due to lack of time.

## How to launch setup
### Prerequisites
 - Installed Docker Engine (https://docs.docker.com/engine/install/)
 - Installed Docker-Compose plugin (https://docs.docker.com/compose/install/)
 - Ports 3000 and 3001 are available
### Build and run
 - Clone the repository
 - launch `docker-compose up` from the root repository folder
 - It will build and launch local setup. The website will be available on http://localhost:3000/
## How to test
I was running out of time, so frontend part tests are absent.
### Prerequisites
 - Node.js 18(LTS) Installed
 - Cloned repository
### Test microservice
 - Got to `microservices/currency-processor` via your terminal
 - Launch `npm install` via your terminal
 - Launch `npm test` via your terminal
You will see the test report directly in your terminal.
Also You can check coverage report in `microservices/currency-processor/coverage`
## How to develop
### General
You need to folow "How to launch setup" guide from above, after "docker-compose" launched your setup, \
frontend part will be available on http://localhost:3000/ \
and microservice will be accesible by http://localhost:3001/ \
You can do any changes after the launch, and that should be automatically applied and deployed on your local setup.
## How to add new node module
 - Add module to the package.json dependencies.
 - Launch `docker-compose down` if it runned
 - Launch `docker-compose up --build` to rebuild docker images with new dependencies

## Additional information
Also, you can develop the app in a regular way, You can go into the needed app folder, launch `npm i` and `npm start` to start an application.